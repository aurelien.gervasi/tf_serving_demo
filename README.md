
## Create conda environment
Conda environment requirements are specified in `env.yml`. To create a `tf2` conda environment : 

```Bash
conda env create -f env.yml
``` 

To activate the environment
```Bash
conda activate tf2
``` 

## Export simple models

To generate several simple models to be served by TF Serving, run :
```Bash
python export_simple_model.py
``` 

## Serve models with TF Serving

Make sure you have Docker installed and running on your machine.

From the repo root directory:

```shell script
docker run -p 8500:8500 -p 8501:8501 --rm --name tf_serving \
--mount type=bind,source=$(pwd)/exported_models/simple_model,target=/models/simple_model \
-e MODEL_NAME=simple_model -t tensorflow/serving &
```

You can specify a custom model configuration file:
```shell script
docker run -p 8500:8500 -p 8501:8501 --rm --name tf_serving \
--mount type=bind,source=$(pwd)/exported_models,target=/models \
-e MODEL_NAME=simple_model -t tensorflow/serving \
--allow_version_labels_for_unavailable_models=true \
--model_config_file=/models/models.config --model_config_file_poll_wait_seconds=10 &
```


## Interact with your model through REST API

Please, see the [REST API TF documentation](https://www.tensorflow.org/tfx/serving/api_rest#predict_api) for all the details.

To get the exposed versions of your model:
```shell script
curl localhost:8501/v1/models/simple_model
```

```json
{
 "model_version_status": [
  {
   "version": "4",
   "state": "AVAILABLE",
   "status": {
    "error_code": "OK",
    "error_message": ""
   }
  }
 ]
}
```

To get the model metadata:
```shell script
curl localhost:8501/v1/models/simple_model/metadata
```
```json
{
"model_spec":{
 "name": "simple_model",
 "signature_name": "",
 "version": "4"
}
,
"metadata": {"signature_def": {
 "signature_def": {
  "serving_default": {
   "inputs": {
    "x": {
     "dtype": "DT_FLOAT",
     "tensor_shape": {
      "dim": [],
      "unknown_rank": false
     },
     "name": "serving_default_x:0"
    }
   },
   "outputs": {
    "output_0": {
     "dtype": "DT_FLOAT",
     "tensor_shape": {
      "dim": [],
      "unknown_rank": false
     },
     "name": "StatefulPartitionedCall:0"
    }
   },
   "method_name": "tensorflow/serving/predict"
  },
  "__saved_model_init_op": {
   "inputs": {},
   "outputs": {
    "__saved_model_init_op": {
     "dtype": "DT_INVALID",
     "tensor_shape": {
      "dim": [],
      "unknown_rank": true
     },
     "name": "NoOp"
    }
   },
   "method_name": ""
  }
 }
}
}
}
```
This is very useful to understand what is exposed by the model.

To run an inference :
```shell script
curl -d '{"instances": [1.0, 2.2]}' -X POST http://localhost:8501/v1/models/simple_model:predict
```
```json
{
    "predictions": [6.0, 8.4
    ]
}
```


## More documentation

### Official doc
- TF Serving [Architecture](https://www.tensorflow.org/tfx/serving/architecture) 
- TF Serving [Model serving config](https://github.com/tensorflow/serving/blob/master/tensorflow_serving/g3doc/serving_config.md)

### Interesting contributions
- Medium blog post explaining gRPC: [link 1](https://medium.com/epigramai/tensorflow-serving-101-pt-1-a79726f7c103), [link 2](https://medium.com/epigramai/tensorflow-serving-101-pt-2-682eaf7469e7))
- Example of gRPC client without TF: [github repo](https://github.com/epigramai/tfserving-python-predict-client)
- Blog post about hacks to improve TF Serving performance: [blog post](https://hackernoon.com/how-we-improved-tensorflow-serving-performance-by-over-70-f21b5dad2d98)

