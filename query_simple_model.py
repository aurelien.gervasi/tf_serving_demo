import json
from time import sleep

import requests
from requests.exceptions import ConnectionError

INPUT = 2
MODEL_NAME = "simple_model"
API_URL = f"http://localhost:8501/v1/models/{MODEL_NAME}:predict"


def make_request(input):
    headers = {'Content-Type': 'application/json'}
    data = json.dumps({
        "instances": [input]
    })
    try:
        response = requests.post(API_URL, data, headers=headers)
    except ConnectionError:
        print("Connection Error")
        return

    if response.status_code == 200:
        print(json.loads(response.content.decode('utf-8')))
    else:
        print("Request failed.")


def main():
    while True:
        make_request(INPUT)
        sleep(0.5)


if __name__ == '__main__':
    main()
