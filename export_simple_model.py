from __future__ import absolute_import, division, print_function, unicode_literals

import os
import shutil

import tensorflow as tf
from tensorflow.keras import Model


class MyBasicModel(Model):
    def __init__(self, input_a):
        super(MyBasicModel, self).__init__()
        self.a = tf.Variable(float(input_a), dtype=tf.float32)

    @tf.function(input_signature=[tf.TensorSpec([], tf.float32)])
    def call(self, x):
        x = tf.math.pow(x, self.a)
        return x


def evaluate_model(model, inputs):
    predictions = model(float(inputs))
    tf.print(predictions)


def export_model(model, export_path):
    tf.saved_model.save(model, export_path)
    print(f'Saved model in {export_path}')


def main():
    model_basedir_path = "exported_models/simple_model"

    for version in range(1, 5):
        model = MyBasicModel(version)
        evaluate_model(model, 2)
        export_path = os.path.join(model_basedir_path, str(version))
        export_model(model, export_path)
        print(f"Exported model version {version} in {export_path}")


if __name__ == '__main__':
    main()
